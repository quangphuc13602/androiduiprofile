package com.example.andrprofileuser;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivitysignin extends AppCompatActivity {
    EditText user, pass;
    Button signinbutton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_signin);
        user = (EditText) findViewById(R.id.EditTextUser);
        pass =  (EditText) findViewById(R.id.EditTextPass);
        signinbutton = findViewById(R.id.buttonsignin);
        signinbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (user.getText().toString().equals("pupu") && pass.getText().toString().equals("1306")) {
                    Intent i = new Intent(MainActivitysignin.this, MainActivity.class);
                    startActivity(i);
                }else if (user.getText().toString().isEmpty() && pass.getText().toString().isEmpty()){
                    Toast.makeText(MainActivitysignin.this, "Nhập tài khoản, mật khẩu.", Toast.LENGTH_SHORT).show();
                    user.requestFocus();
                }
            }
        });
        TextView signup = (TextView) findViewById(R.id.textsignup);
        signup.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view2) {
                Intent i = new Intent(MainActivitysignin.this, MainActivitysignup.class);
                startActivity(i);
            }
        });
    }
}